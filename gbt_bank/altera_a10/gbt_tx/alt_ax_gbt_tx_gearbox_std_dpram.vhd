-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Tx Gearbox DPRAM wrapper
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

--! Custom libraries and packages:
use work.vendor_specific_gbt_bank_package.all;

--! @brief GBT_tx_gearbox_std_dpram - Tx Gearbox DPRAM wrapper
--! @details 
--! The GBT_tx_gearbox_std_dpram module is a generic wrapper to encapsulate the device specific IP
entity gbt_tx_gearbox_std_dpram is
   port (
      
      --=================--
      -- Write interface --
      --=================--
      
      WR_CLK_I                                  : in  std_logic;
		TX_CLKEN_i                                : in  std_logic;
      WR_ADDRESS_I                              : in  std_logic_vector(  2 downto 0);
      WR_DATA_I                                 : in  std_logic_vector(119 downto 0);
      
      --================--
      -- Read interface --
      --================--
      
      RD_CLK_I                                  : in  std_logic;
      RD_ADDRESS_I                              : in  std_logic_vector(  5 downto 0);
      RD_DATA_O                                 : out std_logic_vector( WORD_WIDTH-1 downto 0)
      
   );
end gbt_tx_gearbox_std_dpram;

--! @brief GBT_tx_gearbox_std_dpram architecture - Tx Gearbox DPRAM wrapper
--! @details 
--! The GBT_tx_gearbox_std_dpram module implements the device specific IP.
architecture structural of gbt_tx_gearbox_std_dpram is
  
   --================================ Signal Declarations ================================--
  
   signal writeData                             : std_logic_vector(159 downto 0);
   
	component alt_ax_tx_dpram is
		port (
			data      : in  std_logic_vector(159 downto 0) := (others => 'X'); -- datain
			wraddress : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- wraddress
			rdaddress : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rdaddress
			wren      : in  std_logic                      := 'X';             -- wren
			wrclock   : in  std_logic                      := 'X';             -- wrclock
			rdclock   : in  std_logic                      := 'X';             -- rdclock
			q         : out std_logic_vector(WORD_WIDTH-1 downto 0)                      -- dataout
		);
	end component alt_ax_tx_dpram;
   --=====================================================================================--
  
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== User Logic =====================================--

   writeData                                    <= x"0000000000" & WR_DATA_I;

   -- Comment: The "Q" output port of "dpram" is registered internally.
   
   dpram: alt_ax_tx_dpram
      port map (
         WREN                                   => TX_CLKEN_i,
         WRCLOCK                                => WR_CLK_I,
         RDCLOCK                                => RD_CLK_I,
         DATA                                   => writeData,
         RDADDRESS                              => RD_ADDRESS_I,
         WRADDRESS                              => WR_ADDRESS_I,
         Q                                      => RD_DATA_O
      );

   --=====================================================================================--         
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
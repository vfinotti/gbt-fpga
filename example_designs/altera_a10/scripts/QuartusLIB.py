#!/usr/bin/env python
import tempfile
import shutil
import subprocess
import sys
import time
import glob
import os
import re
import inspect

from Queue import Queue, Empty
from threading  import Thread

# -------------------------------------------------------------
#  ------------------------- Class Vivado ---------------------
# -------------------------------------------------------------

class QuartusLIB:

    def __init__(self, projectDir, projectName):
        self.project_file = "%s/%s" %(projectDir, projectName)
        self.project_dir = projectDir
        self.project_name = projectName

    def copyBitFile(self, dest):
        for file in os.listdir("%s/output_files" %(self.project_dir)):
            if file.endswith(".sof"):
                print("[Success] Copy %s/output_files/%s to %s" %(self.project_dir, file, dest))
                shutil.copyfile("%s/output_files/%s" %(self.project_dir, file), dest)

    def enqueue_output(self, out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
    
    def runCommand(self, command, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):

        logFile = open(logFileName,"wb")

        start = time.time()
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

        stdoutQueue = Queue()
        stderrQueue = Queue()
        
        stdoutThread = Thread(target=self.enqueue_output, args=(process.stdout, stdoutQueue))
        stdoutThread.daemon = True # thread dies with the program
        stdoutThread.start()
        
        stderrThread = Thread(target=self.enqueue_output, args=(process.stderr, stderrQueue))
        stderrThread.daemon = True # thread dies with the program
        stderrThread.start()

        i = 0

        while True:
                
            if stdoutThread.isAlive() == False and stderrThread.isAlive() == False:
                break

            try:  nextStderrline = stderrQueue.get_nowait() # or q.get(timeout=.1)
            except Empty:
                pass #Null: nothing to do
            else:
                end = time.time()
                ellapsed_time = "(Elapsed: %fs)     " %((end-start))
                sys.stdout.write("\r[On-going] Quartus command " + "." * i + " " * (10-i) + ellapsed_time)
                sys.stdout.flush()

                i = i+1
                if i >= 10:
                    i = 0
                    
                if enErrorVerb:
                    sys.stdout.write("\r                             " + " " * i + " " * (10-i) + "                      ")
                    sys.stdout.write("\r\t\033[1;91m* [STDERR] ERROR: %s\033[0m" %(nextStderrline))
                    
                logFile.write(nextStderrline)

            try:  nextStdoutline = stdoutQueue.get_nowait() # or q.get(timeout=.1)
            except Empty:
                pass     #Null: nothing to do
            else:
                end = time.time()
                ellapsed_time = "(Elapsed: %fs)     " %((end-start))
                sys.stdout.write("\r[On-going] Quartus command " + "." * i + " " * (10-i) + ellapsed_time)
                sys.stdout.flush()

                i = i+1
                if i >= 10:
                    i = 0
                    
                if "Error" in nextStdoutline[0:20] and enErrorVerb:
                    sys.stdout.write("\r                             " + " " * i + " " * (10-i) + "                      ")
                    sys.stdout.write("\r\t\033[1;91m* %s\033[0m" %(nextStdoutline))
                    
                if "Warning" in nextStdoutline[0:20] and enWarningVerb:
                    sys.stdout.write("\r                             " + " " * i + " " * (10-i) + "                      ")
                    sys.stdout.write("\r\t\033[93m* %s\033[0m" %(nextStdoutline))
                    
                if "Info" in nextStdoutline[0:20]  and enInfoVerb:
                    sys.stdout.write("\r                             " + " " * i + " " * (10-i) + "                      ")
                    sys.stdout.write("\r\t\033[94m* %s\033[0m" %(nextStdoutline))

                if "Error" not in nextStdoutline[0:20] and "Warning" not in nextStdoutline[0:20] and "Info" not in nextStdoutline[0:20] and not nextStdoutline.strip() and enStatusVerb:
                    sys.stdout.write("\r                             " + " " * i + " " * (10-i) + "                      ")
                    sys.stdout.write("\r\t# %s" %(nextStdoutline))
                    
                logFile.write(nextStdoutline)

        logFile.close()
        
        end = time.time()
        ellapsed_time = "(Elapsed: %fs)     " %((end-start))
        sys.stdout.write("\r[Finished] Quartus command " + "." * i + " " * (10-i) + ellapsed_time + "\n")

        stdoutThread.join()
        stderrThread.join()
        
        tmp = process.communicate()[0]
        return process.returncode
        
    def compile(self, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):

        command = "quartus_sh --flow compile \"%s\"" %(self.project_file)
        print("[Success] Run Quartus command: %s" %(command))
        return self.runCommand(command, logFileName, enStatusVerb, enInfoVerb, enWarningVerb, enCriticalWarningVerb, enErrorVerb)
    
    def timing(self, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):

        command = "quartus_sta \"%s\"" %(self.project_file)
        print("[Success] Run Quartus command: %s" %(command))
        execCode = self.runCommand(command, logFileName, enStatusVerb, enInfoVerb, enWarningVerb, enCriticalWarningVerb, enErrorVerb)

        if execCode != 0:
            return execCode
        
        with open(logFileName) as f:
            content = f.readlines()

            for line in content:
                m = re.search(r"Info \(([A-Za-z0-9_]+)\):", line)

                if m:
                    infoCode = int(m.group(1))

                    if infoCode == 332146:
                        #Analyze name
                        inf, details = line.split(':', 1)
                        slackSearch = re.search(r"(-?[0-9.]+)", details)

                        if slackSearch:
                            slack = slackSearch.group(1)

                            if '-' in slack:
                                sys.stdout.write("\033[1;91mError: %s \033[0m" %(details))

                    if infoCode == 332119:
                        #Result
                        inf, details = line.split(':', 1)
                        if details:
                            params = details.split()
                            
                            signal = params[2]
                            slack = params[0]
                            tns = params[1]

                            if '-' in slack or '-' in tns:
                                print("\t\033[1m[%s] Slack issue on signal %s [End point TNS: %s] \033[0m" %(slack, signal, tns))
                                execCode = -1
                            

        return execCode

    def program(self, bitfile, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):
        command = "quartus_pgm -m JTAG -o \"p;%s\"" %(bitfile)
        print("[Success] Run Quartus command: %s" %(command))
        return self.runCommand(command, logFileName, enStatusVerb, enInfoVerb, enWarningVerb, enCriticalWarningVerb, enErrorVerb)

    def systemConsole(self, tclFile, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):
        command = "system-console -cli --script \"%s\"" %(tclFile)
        print("[Success] Run system-console command: %s" %(command))
        return self.runCommand(command, logFileName, enStatusVerb, enInfoVerb, enWarningVerb, enCriticalWarningVerb, enErrorVerb)

    def runTCPServerInThread(self, logFileName, enStatusVerb = True, enInfoVerb = True, enWarningVerb = True, enCriticalWarningVerb = True, enErrorVerb = True):
        scriptPath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        
        hwServThread = Thread(target=self.systemConsole, args=("%s\\altera_tcpserv.tcl" %(scriptPath), logFileName, enStatusVerb, enInfoVerb, enWarningVerb, enCriticalWarningVerb, enErrorVerb))
        hwServThread.daemon = True # thread dies with the program
        hwServThread.start()
    
        return hwServThread
        

        
